// pcake a package for managing your pancakes

package pcake

import (
	"bufio"
	"errors"
	"fmt"
	"io"
	"strconv"
)

var (
	// The first line provided to SortLines wasn't an integer
	ErrLineNotAnInt = errors.New("first line isn't an int")
)

// Sort organizes your flapjacks with a smiley face up
func Sort(pattern string) int {
	flipCount := 0
	parts := []byte(pattern)
	n := len(parts)

	for i := 1; i < n; i++ {
		if parts[i] != parts[i-1] {
			flipCount++
		}
	}

	if parts[n-1] == '-' {
		flipCount++
	}

	return flipCount
}

// SortLines reads lines from a reader and outputs counts for lines
func SortLines(r io.Reader) ([]int, error) {
	var sortNums []int
	s := bufio.NewScanner(r)

	line := 0
	for s.Scan() {
		// if line 0 check for integer
		if line == 0 {
			intVal, err := strconv.Atoi(s.Text())
			if err != nil || intVal == 0 {
				return nil, ErrLineNotAnInt
			}
			line = 1
			continue
		}

		num := Sort(s.Text())
		fmt.Printf("Case #%d: %d\n", line, num)
		sortNums = append(sortNums, num)
		line++
	}

	return sortNums, nil
}
