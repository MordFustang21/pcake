package pcake

import (
	"bytes"
	"os"
	"testing"
)

var testData = []struct {
	pancakes string
	numFlips int
}{
	{"7", 0},
	{"-", 1},
	{"+", 0},
	{"+-", 2},
	{"--", 1},
	{"-+", 1},
	{"--+++--", 3},
	{"+-+-+-", 6},
}

func TestSort(t *testing.T) {
	for _, test := range testData {
		num := Sort(test.pancakes)
		if num != test.numFlips {
			t.Errorf("Expected %d got %d", test.numFlips, num)
		}
	}
}

var lineResults = []int{2, 1, 3, 2}

func TestSortLines(t *testing.T) {
	f, err := os.Open("testdata/pancakes.txt")
	if err != nil {
		t.Errorf("Failed to open file: %s", err)
	}

	res, err := SortLines(f)
	if err != nil {
		t.Fatal("Error sorting lines", err)
	}

	for index, val := range res {
		if lineResults[index] != val {
			t.Errorf("Expected %d got %d", lineResults[index], val)
		}
	}

	buf := bytes.NewBufferString(`4
+-
-+
--+++--
++--++`)
	res, err = SortLines(buf)
	if err != nil {
		t.Fatal("Error sorting lines", err)
	}

	for index, val := range res {
		if lineResults[index] != val {
			t.Errorf("Expected %d got %d", lineResults[index], val)
		}
	}
}

func TestSortLinesError(t *testing.T) {
	buf := bytes.NewBufferString(`+-
++
--`)

	_, err := SortLines(buf)
	if err == nil {
		t.Fatal("Error expected got nil")
	}

	if err != ErrLineNotAnInt {
		t.Fatal("Error not of type ErrLineNotAnInt")
	}
}
